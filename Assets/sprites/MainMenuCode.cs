﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuCode : MonoBehaviour {


    public Button start;
    public Button quit;

	void Start () {
        start.GetComponent<Button>().onClick.AddListener(TaskOnClick);
        quit.GetComponent<Button>().onClick.AddListener(TaskOnClick);
    }

    void Update()
    {
        
    }

    void TaskOnClick()
    {
        SceneManager.LoadScene("level1");
       
    }
    void escape()
    {
        Application.Quit();
    }
}
