﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pointer : MonoBehaviour {

    public GameObject player;
	// Update is called once per frame
	void Update () {


        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 8.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * -8.0f;
        if(x==0 && z == 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position,8);
        }
        transform.Translate(new Vector3(x, player.transform.position.y, z));

    }
}
