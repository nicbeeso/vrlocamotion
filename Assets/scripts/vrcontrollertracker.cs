﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class vrcontrollertracker : MonoBehaviour
{


    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
    private SteamVR_TrackedObject trackedObj;
   
    public Vector3 controllerVel;
    private int score = 0;
    bool bump1 = false;
    bool bump2 = false;
    bool isWaving = false;

    public Vector2 Axis;
    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    public bool triggerButtonDown = false;
    public bool triggerButtonUp = false;
    public bool triggerButtonPressed = false;

    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    public bool gripButtonDown = false;
    public bool gripButtonUp = false;
    public bool gripButtonPressed = false;

    // Use this for initialization
    void Start()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }
    public int getScore()
    {
        return score;
    }
    public Vector2 GetAxis()
    {
        return Axis;
    }
    void Update()
    {

        Axis = controller.GetAxis();
       
        //reference to the tracked-object component on the controller/HMD GameObject

        triggerButtonPressed = controller.GetPress(triggerButton);
        gripButtonPressed = controller.GetPress(gripButton);

        if (triggerButtonPressed)
        {
            //make fist
          

        }
        else
        {
           
            //no fist
        }
        if (gripButtonPressed)
        {
         
        }
        //get the device associated with that tracked object (which is how you access buttons and stuff)
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)trackedObj.index);

        //get the velocity
        Vector3 vel = device.velocity;
        controllerVel = vel;
        //  debug.text = vel.ToString();

        if (vel.z > 0.2)
        {
            bump1 = true;
        }
        else if (vel.z < -.2)
        {
            bump2 = true;
        }
        if (bump1 == true && bump2 == true)
        {
            score++;
            isWaving = true;
            bump1 = false;
            bump2 = false;
        }
        else
        {
            isWaving = false;
        }
    }

    public bool getIsWaving()
    {
        return isWaving;
    }

}
