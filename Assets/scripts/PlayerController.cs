﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    //vr CONTROLS
    public Vector2 Axis;
    public GameObject cntrller;
    private Collider C;
    public vrcontrollertracker left;
    public vrcontrollertracker right;




    //public GameObject pointer;

    public float jumpForce;
    public bool grounded;
    public string ground;
    public int lives;
    bool canJump;
    public Text timer;
    public Text coinage;
    public Text life;
    private float time;
    bool starttime;
    public int coins = 0;
    private bool allcoins;
    private Scene current;

    //anim bool
    private bool bwalkfor = false;


    
    public Animator anim;

	void Start () {
       
        allcoins = false;
        canJump = true;
        time = 0;
        starttime = false;
        anim.speed = 1.5f;
        
        current =  SceneManager.GetActiveScene();

     
    }


    

    IEnumerator wait()
    {
        GetComponent<Rigidbody>().velocity += jumpForce * Vector3.up;
        canJump = true;
        yield return new WaitForSeconds(1);
        
    }
    IEnumerator nextLevel()

    {
        yield return new WaitForEndOfFrame();
        if (current.name == "level1" && lives != 0) SceneManager.LoadScene("level2");
        else SceneManager.LoadScene("mainmenu");
       
    }
    IEnumerator endLevel()
    {
        //play death sound?
        Debug.Log(current.name + " " + coins);
        
            
            

        
        if (lives<=0)
        {
            if (current.name == "level1") SceneManager.LoadScene("level1");
            if (current.name == "level2") SceneManager.LoadScene("level2");
        }
        yield return new WaitForEndOfFrame();


    }
	
	void Update()
    {

   



     
        if (lives == 3) life.text = "Lives:III";
        if (lives == 2) life.text = "Lives:II";
        if (lives == 1) life.text = "Lives:I";

        if (left.triggerButtonPressed && grounded )
        {
            Debug.Log("jump");
            canJump = true;
            anim.SetBool("jump", true);
        }
      if(starttime && !allcoins)  time += Time.deltaTime;
        timer.text =time+"";
        coinage.text = coins + "/5";
        if (coins == 5)
        {
            allcoins = true;

            StartCoroutine(nextLevel());
        }

    }
	void FixedUpdate () {
        transform.localScale = transform.localScale;

       var x = Input.GetAxis("Horizontal") * Time.deltaTime * 4.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * -4.0f;
       
        //animation
        if (x == 0 && z == 0)
        {
            if (bwalkfor)
            {
                anim.SetBool("idleback", false);
                anim.SetBool("idlefor", true);
                //anim.SetBool("walkside",false);
            }
            else {
                anim.SetBool("idlefor", false);
                anim.SetBool("idleback", true);
            }
            anim.SetBool("walkfor", false);
            anim.SetBool("walkback", false);
        }
        else if (z >= 0 && Mathf.Abs(x)<.5f)
        {
            bwalkfor = true;
            anim.SetBool("idlefor", false);
            anim.SetBool("idleback", false);
            anim.SetBool("walkfor", true);
        }
        else if (z < 0) //back
        {
           
            bwalkfor = false;
            anim.SetBool("idlefor", false);
            anim.SetBool("idleback", false);
            anim.SetBool("walkback", true);
        }
        else if (x != 0 && z>.5f)
        {
            anim.SetBool("walkside", true);
            anim.SetBool("idlefor", false);
        }
            if (Input.GetAxis("Vertical") != 0)
        {
            if(Input.GetAxis("Vertical")>0) 
            starttime = true;
        }
        if (transform.position.y < -5)
        {
            lives = 0;
        }
        if (canJump)
        {
            canJump = false;
            GetComponent<Rigidbody>().velocity += jumpForce * Vector3.up;
            GetComponent<AudioSource>().Play();
            
        }

       

      
        transform.Translate(x, 0, z,Space.World);
        if(x<0)
        transform.eulerAngles = new Vector3(0, 90, 0);
        if (x > 0)
            transform.eulerAngles = new Vector3(0, -90, 0);
        if (z > 0)
            transform.eulerAngles = new Vector3(0, 180, 0);
        if (z < 0)
            transform.eulerAngles = new Vector3(0, 0, 0);

      


        if (ground == "yes")
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
        if (GetComponent<Rigidbody>().velocity.y == 0)
        {
            anim.SetBool("jump", false);
            grounded = true;
        }

        if (lives <=0) StartCoroutine(endLevel());
    }

    public void addCoin()
    {
        
        coins++;
    }
   
    void OnCollisionEnter(Collision c)
    {
      

        if (c.gameObject.tag == "ground")
        {
            transform.parent = c.transform;
            ground = "yes";
        }
        if (c.gameObject.tag == "tramp")
        {
            GetComponent<AudioSource>().Play();
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 900, 0));
        }
        if (c.gameObject.tag == "enemy")
        {
            if (transform.position.y > c.gameObject.transform.position.y) Debug.Log("boop");
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 200, 0));
            lives--;
        }
    }

    void OnCollisionExit(Collision c)
    {
        transform.parent = null;
        
        if (c.gameObject.tag == "ground")
        {
            
            ground="no";
        }
    }

}
