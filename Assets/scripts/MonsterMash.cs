﻿using UnityEngine;
using System.Collections;

public class MonsterMash : MonoBehaviour {


    public Vector3[] points = new Vector3[4];
    public GameObject player;

    public float rate;
    private int count = 0;
    public float step;
    private bool patrol = true;


    void FixedUpdate()
    {
        if (patrol)
        {
            transform.position = Vector3.MoveTowards(transform.position, points[count], step);

            if (transform.position == points[count])
            {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
                if (count < points.Length - 1)
                {
                    count++;
                }
                else {
                    count = 0;
                }
            }


        }
        else
        {
            if (patrol == false)
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
        }



    }


    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            Debug.Log("kjdfkljdssdl");
            patrol = false;


        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            Debug.Log("balls");
            patrol = true;


        }
    }
}
