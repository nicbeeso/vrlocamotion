﻿using UnityEngine;
using System.Collections;

public class movingPlat : MonoBehaviour {


    public float speed;
   
    private int count;

    public GameObject button;
    public Transform[] points;
    bool b;

    bool waiting;
    bool hold;
	void Start () {
        count = 0;
        Debug.Log(points.Length);
        b = true;
        waiting = false;
        hold = false;
    }
    IEnumerator wait()
    {
        Debug.Log("waiting..");
        hold = true;
        yield return new WaitForSeconds(.5f);
        waiting = false;
        hold = false;
    }

    // Update is called once per frame
    void Update()
    {
       
        if (button == null || button.GetComponent<button>().getPressed())
        {
            transform.position = Vector3.MoveTowards(transform.position, points[count].position, speed);

            if (transform.position == points[count].position)
            {
                waiting = true;

             //   if (waiting && hold == false) StartCoroutine(wait());
                if (count < points.Length-1 && b && hold!=true)
                {
                    count++;
                   
                }
                else {
                    count = 0;
                }
                b = false;
            }
            else
            {
               b =true;
            }

        }
    }
}
