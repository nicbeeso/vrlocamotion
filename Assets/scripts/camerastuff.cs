﻿using UnityEngine;
using System.Collections;

public class camerastuff : MonoBehaviour {

    private Color original;

    void Start()
    {
        original = GetComponent<Renderer>().material.color;
    }

	void OnTriggerEnter(Collider c)
    {
        if(c.tag =="MainCamera") GetComponent<Renderer>().material.color =
              new Color(original.r, original.g,original.b, 0.4f); Debug.Log("touched");

    }
    void OnTriggerExit(Collider c)
    {
        if (c.tag == "MainCamera") GetComponent<Renderer>().material.color =
               original;
    }

}
