﻿using UnityEngine;
using System.Collections;

public class coin : MonoBehaviour {

    public float rotSpeed;
    public GameObject effect;

    IEnumerator bling()
    {
        GetComponent<AudioSource>().Play();
        Instantiate(effect, this.transform.position, Quaternion.identity);
        //instantiate partile effect
        yield return new WaitForSeconds(.5f);
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Player")
        {
            c.gameObject.GetComponent<PlayerController>().addCoin();
            StartCoroutine(bling());
        }
    }
	
	// Update is called once per frame
	void Update () {
     
        transform.Rotate(Vector3.left * Time.deltaTime * rotSpeed);
    }
}
