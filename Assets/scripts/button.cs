﻿using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {

    private bool pressed;
    public GameObject piece;
    public bool vrbutton;

	void Start () {
        pressed = false;
	}

    public bool getPressed()
    {
        return pressed;
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Player" && !pressed && !vrbutton)
        {
            pressed = true;
            piece.transform.position = new Vector3(piece.transform.position.x, piece.transform.position.y - .15f, piece.transform.position.z);
        }
       
    }
    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "controller" && !pressed)
        {
            pressed = true;
            piece.transform.position = new Vector3(piece.transform.position.x, piece.transform.position.y - .15f, piece.transform.position.z);
        }
    }

	
	
}
